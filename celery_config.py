from celery import Celery

celery = Celery('celery_config', broker='redis://localhost:6379/0', include=['tasks'])